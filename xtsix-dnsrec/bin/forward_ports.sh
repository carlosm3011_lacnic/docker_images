#!/bin/bash
for i in {50000..50999}; do
	echo -n "Forwarding port $i... "
	VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port$i,tcp,,$i,,$i";
	VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port$i,udp,,$i,,$i";
	echo "done!"
done
